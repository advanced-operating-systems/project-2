//Add your code here
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <assert.h>
#define STR_EQ(x, y) !strcmp(x, y)

static char PROMPT[]  = "wish> ";
static char* searchPaths[2] = {"/bin", NULL};
static char shellPaths[256][2048];

struct Command {
  char **args; // ["ls", "-f"]
  char* outputFile;
  int numArgs; //["ls", "test"] -> 2
};

// static int MAXPATHLEN = 200;
void quick_print(int i) {
    printf("hello %d\n", i);
}

void maybe_print_error() {
    if (0) {
        char* err = strerror(errno);
        printf("[WISH INTERNAL ERROR]: %s\n", err);
    }
}
static void clear_shell_paths() {
  int i;
  for (i = 0; i < 256; ++i) {
    memset(shellPaths[i], 0, 2048 * sizeof(char));
  }
}
/* Frees an array of strings when given the length as well. */
void free_array(char** array, int len){
  int i;
  for(i = 0; i < len; i++){
    free(array[i]);
  }
  free(array);
}
int set_shell_path(char **newPaths) {
  if (!newPaths) {
    return 0; /* Assume this was an error. To clear the shell paths, pass in a
                 pointer-to-nullpointer, not a nullpointer directly. */
  }
  clear_shell_paths();
  int i;
  for (i = 0; i < 256 && newPaths[i]; ++i) {
    if (strlen(newPaths[i]) + 1 > 2048) {
      return 0; /* This path is too long. */
    }
    strcpy(shellPaths[i], newPaths[i]);
  }
  return 1;
}

static void joinpath(const char* dirname, const char* basename, char* buf) {
  assert(dirname && "Got NULL directory name in joinpath.");
  assert(basename && "Got NULL filename in joinpath.");
  assert(buf && "Got NULL output in joinpath");
  size_t dlen = strlen(dirname);

  strcpy(buf, dirname);
  strcpy(buf + dlen + 1, basename);
  buf[dlen] = '/';
}

void Closedir(DIR* dirp) {
  if (closedir(dirp) == -1 && 0) {
    printf("[WISH_INTERNAL]: Error closing directory.\n");
  }
}

char* exeExistsInDir(const char* dirname, const char* filename) {
  if (!dirname || !filename) {
    return NULL;
  }
  DIR* dir;
  struct dirent* dent;
  dir = opendir(dirname);
  if (!dir) {
    maybe_print_error();
    return NULL;
  }

  errno = 0;
  while ((dent = readdir(dir))) {
    if (!strcmp(dent->d_name, filename)) {
      size_t buflen = strlen(dirname) + strlen(filename) + 2;
      char* buf = malloc(buflen*  sizeof(char));
      if (!buf) {
        maybe_print_error();
        return NULL;
      }
      joinpath(dirname, filename, buf);
      int exec_forbidden = access(buf, X_OK);
      if (!exec_forbidden) {
        Closedir(dir);
        return buf;
      } else {
        switch (errno) {
        case EACCES:
        case ENOENT:
        case ENOTDIR:
          errno = 0;
          break;
        case EIO:
        case EINVAL:
        case EFAULT:
        case ENOMEM:
        case ETXTBSY:
        case EROFS:
        case ENAMETOOLONG:
        case ELOOP:
          maybe_print_error();
          errno = 0;
        }
        free(buf);
      }
    }
  }
  if (errno == EBADF) {
    maybe_print_error();
  }
  Closedir(dir);
  return NULL;
}


void add_paths(char** path) {
    // int nread = 0;
    // int i = 0;
    // while(&path[i] != '\0') {
    //     nread++;
    //     printf("char %s\n", &path[i]);
    // }
}

int is_absolute_path(char* path) {
  if (!path) {
    return 0;
  }
  return* path == '/';
}

int getLen(char** s) {
    int count = 0;
    while(s[count] != NULL) {
        count ++;
    }
    return count;
}

int getNread(char* s) {
    int nread = 0;
    while(s[nread] != '\0') {
        nread ++;
    }
    return nread;
}

void print_error(){
    char error_message[30] = "An error has occurred\n";
    write(STDERR_FILENO, error_message, strlen(error_message));
}


char** splitCommands(char* line, int* numOfCmds) {
    // char* cmdCopy = (char*) malloc(nread + 1);
    line[strlen(line)-1] = '\0';
    char* cmdCopy = strdup(line);

    // Extract the first token
    // char*  token = strtok(cmdCopy, "\n");
    char* token = strtok(cmdCopy, "&");

    // loop through the string to extract all other tokens
    int cmdTokenCount = 0;
    while( token != NULL ) {
        token = strtok(NULL, "&");
        cmdTokenCount++;
    }
    free(cmdCopy);

    *numOfCmds = cmdTokenCount;
    if(cmdTokenCount <= 0) {
        free(line);
        return NULL;
    }
    char** cmdArr = malloc((cmdTokenCount) * sizeof(char*));

    // Extract the first token
    // token = strtok(line, "\n");
    token = strtok(line, "&");
    // loop through the string to extract all other tokens
    int i = 0;
    while( token != NULL ) {
        // cmdArr[i] = token;
        cmdArr[i] = strdup(token);
        // cmdArr[i] = (char* ) malloc((strlen(token) + 1) * sizeof(char));
        // strcpy(cmdArr[i], token);

        token = strtok(NULL, "&");
        i++;
    }

    // printf(" asdasda *%s* *%s* \n", cmdArr[0], cmdArr[1]);
    free(line);

    return cmdArr;
}

struct Command* parseCommand(char* cmdline, size_t nread) {

    struct Command* cmd;
    cmd = malloc(1 * sizeof(struct Command));
    cmd->args = NULL;
    cmd->outputFile = NULL;
    cmd->numArgs = 0;
    // int cmdLeftRightNumOfTok = 0;

    int totalLength = strlen(cmdline);
    char* cmdLeft = strtok(cmdline, ">");
    int hasOutputFile = totalLength != strlen(cmdLeft);
    if (hasOutputFile) {
        char* cmdRight = strtok(NULL, ">");
        if(strtok(NULL, ">") != NULL) { // if there is another >
            print_error();
            exit(0);
        }

        //remove spaces from right (outputfile)
        if( cmdRight != NULL ) {
            char* outfile = strtok(cmdRight, " ");
            if( outfile != NULL ) {
                if(strtok(NULL, " ") != NULL) { // check if there are multiple output files
                    print_error();
                    exit(0);
                }
                cmd->outputFile = strdup(outfile);

            } else {
                print_error();
                exit(0);
            }
        } else {
            // error
            print_error();
            exit(0);
        }
    }

    //remove spaces from left
    nread = strlen(cmdLeft);
    // char* cmdLeftCopy = (char*) malloc(nread + 1);
    char* cmdLeftCopy = strdup(cmdLeft);
    char* arg = strtok(cmdLeftCopy, " ");
    int cmdLeftTokenCount = 0;
    while( arg != NULL ) {
        cmdLeftTokenCount++;
        arg = strtok(NULL, " ");
    }
    cmd->numArgs = cmdLeftTokenCount;

    char** cmdArr = malloc((cmdLeftTokenCount+1) * sizeof(char*));
    arg = strtok(cmdLeft, " ");
    int i = 0;
    while( arg != NULL ) {
        // cmdArr[i] = arg;
        cmdArr[i] = strdup(arg);
        arg = strtok(NULL, " ");
        i++;
    }

    cmdArr[i] = NULL;
    cmd->args = cmdArr;

    free(cmdLeft);
    free(cmdLeftCopy);
    return cmd;
}


// if first command is builtin, ignore if it has 1+ commands, just execute the first one return 1
// else return 0

void execExternalCmd(struct Command* cmd) {
    // printf("HERE being: \n");
    // execv("/bin/ls", cmd->args);
    char* exec = NULL;
    if(!is_absolute_path(cmd->args[0])){
        int x = 0;
        /* Checks if the requested external program exists in each known path. */
        while(!(exec = exeExistsInDir(shellPaths[x], cmd->args[0])) && 
            strcmp(shellPaths[x], "") != 0){
            x++;
        }
        if(exec == NULL){
            print_error();
            return;
        }
    } 
    else {
        exec = cmd->args[0];
        int exec_forbidden = access(exec, X_OK);
        if (exec_forbidden) {
            print_error();
            // free(exec);
            return;
        }
    }

    if(cmd->outputFile != NULL) {
        int fd = open(cmd->outputFile, O_WRONLY | O_CREAT | O_TRUNC, 0666);
        if(fd == -1){
            print_error();
            return;
        }
        if(dup2(fd, 1) == -1){
            print_error();
            return;
        }
        if(dup2(fd, 2) == -1){
            print_error();
            return;
        }
    }
    // printf("HERE 1111\n");
    execv(exec, cmd->args);
    // printf("HERE 2\n");
    print_error();
    // exit(1);


    
    return;
}

void execExternalCmds(struct Command* cmds[], int numCommands, int i) {
    /* Base case */
    pid_t fork_value;

    for (int i = 0; i < numCommands; i++ ) {
        fork_value = fork();

        /*if child, execute individual program*/
        if(fork_value < 0){
            print_error();
        }
        if(fork_value == 0){
            execExternalCmd(cmds[i]);
            exit(0);
        }


        /*if parent, handle the next command in the array with recursion.*/
    }

    if(fork_value > 0) { // is the parent
        int status = 0;
        // pid_t childPid = wait(&status);
        while(wait(&status) > 0) {

        }
    }
}

int tryExecBuiltin(struct Command* cmd) { // will only execute the first command, and return 1 to inform the parent method that this is built in
    if(cmd == NULL) { // bad command
        return 1;
    }
    char** cmdArr = cmd->args;
    if(strcmp(cmdArr[0], "cd") == 0 || strcmp(cmdArr[0], "exit") == 0 || strcmp(cmdArr[0], "path") == 0 ) { //build-in command
        if(strncmp(cmdArr[0], "exit", 4) == 0){
            if(cmd->numArgs != 1){
                print_error();
            } else {
                exit(0);
            }
        } else if(strncmp(cmdArr[0], "cd", 2) == 0){
            if(cmd->numArgs != 2){
                print_error();
            } else {
                if(chdir(cmdArr[1]) == -1){
                    print_error();
                }
            }
            return 1;
        } else if(strncmp(cmdArr[0], "path", 4) == 0){
            set_shell_path(&cmdArr[1]);
        }
        return 1;
    } else { // external command
        return 0;
    }
}

int main(int argc, char* argv[]) {
    FILE* stream = NULL;
    char* printedPrompt = NULL;
    char emptyPrompt[] = "";

    set_shell_path(searchPaths);
    //batch mode
    if(argc == 1){
        printedPrompt = PROMPT;
        stream = stdin;
    } else if(argc == 2){
        printedPrompt = emptyPrompt;
        stream = fopen(argv[1], "r");
        if(stream == NULL){
            print_error();
            exit(1);
        }
    } else {
        print_error();
        exit(1);
    }

    //TODO: interactive mode
    while (1) {
        printf("%s", printedPrompt);

        // read prompt
        char* line =NULL;

        size_t len = 0;

        size_t nread = 0;
        
        if ((nread = getline(&line, &len, stream)) != -1) {
            if(!line) {
                print_error();
                exit(1);
            }
        } else {
            // printf("\n");
            exit(0);
        }
        // split command
        if(nread == 1) {
            free(line);
            continue;
        }
        // printf("line *%s*\n", line);
        int numCommands = 0;
        char** cmds = splitCommands(line, &numCommands);
        // get an array of Command strucs
        if(cmds == NULL) {
            continue;
        }
        struct Command* structCommands[numCommands];

        int i;
        for (i = 0; i< numCommands; i++ ) {
            nread = strlen(cmds[i]);
            char* cmdstr = (char*) malloc(nread+1);
            strcpy(cmdstr, cmds[i]);
            struct Command* structCommand = parseCommand(cmdstr, nread);

            if (structCommand->numArgs == 0) {
                free(structCommand->args);
                free(structCommand);
                structCommands[i] = NULL;
            }

            // if(structCommand.args != NULL){
            //     /* Add valid commands to the array */
            //     validCommands[numValidCommands] = cmd;
            //     numValidCommands++;
            // } else {
            //     /* Invalid commands */
            //     free_array(tokens, numTokens);
            // }



            // printf(" structCommand *%s* *%s* *%s*\n",structCommand->args[0], structCommand->args[1], structCommand->args[2]);
            else {
                structCommands[i] = structCommand;
            }
        }

        // execute commands
        if(tryExecBuiltin(structCommands[0]) == 0) {
            // 
            execExternalCmds(structCommands, numCommands, 0);
        } else {
            // for the rest
            for(i = 1; i < numCommands; i++) { 
                tryExecBuiltin(structCommands[i]);
            }
            
        }
        
        /* TODO Free commands */
        // free(line);
        free_array(cmds, numCommands);
        // free(cmds[0]);
        for(i = 0; i < numCommands; i++) {      
            if(structCommands[i] != NULL) {
                if(structCommands[i]->args != NULL) {
                    free_array(structCommands[i]->args, structCommands[i]->numArgs);
                }
                if(structCommands[i]->outputFile != NULL) {
                    free(structCommands[i]->outputFile );
                }
                free(structCommands[i]);
            }
        }
    }
    return 0;
    
}