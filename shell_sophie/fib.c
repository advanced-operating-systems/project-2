/*
  fib - A program that compute fibonacci number, range [0, 13]

  Tanner Dreiling, trd758
  Xuefei Zhao, xz6784

  9/7/2020 - 9/17/2020
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>

const int MAX = 13;

static void doFib(int n, int doPrint);


/*
 * unix_error - unix-style error routine.
 */
inline static void unix_error(char *msg)
{
    fprintf(stdout, "%s: %s\n", msg, strerror(errno));
    exit(1);
}


int main(int argc, char **argv)
{
    int arg;
    int print=1;

    if(argc != 2){
        fprintf(stderr, "Usage: fib <num>\n");
        exit(-1);
    }

    arg = atoi(argv[1]);
    if(arg < 0 || arg > MAX){
        fprintf(stderr, "number must be between 0 and %d\n", MAX);
        exit(-1);
    }

    doFib(arg, print);

    return 0;
}


/* 
 * Recursively compute the specified number. If print is
 * true, print it. Otherwise, provide it to my parent process.
 *
 * NOTE: The solution must be recursive and it must fork
 * a new child for each call. Each process should call
 * doFib() exactly once.
 */
static void doFib(int n, int doPrint)
{
    /* Tanner and Sophie both driving */
    int result;
    /* Base case */
    if(n == 0 || n == 1){
        result = n;
    } else {
        /* Child 1 */
        pid_t forkVal1 = fork();
        if(forkVal1 < 0){
            unix_error("fork error");
        } else if(forkVal1 == 0){
            doFib(n - 1, 0);
        } else {
            int status1;
            pid_t child1Pid = wait(&status1);
            if(child1Pid == -1){
                unix_error("could not wait");
            }
            int value1 = WEXITSTATUS(status1);
            
            /* Child 2 */
            pid_t forkVal2 = fork();
            if(forkVal2 < 0){
                unix_error("fork error");
            } else if(forkVal2 == 0){
                doFib(n - 2, 0);
            } else {
                /* Parent */
                int status2;
                pid_t child2Pid = wait(&status2);
                if(child2Pid == -1){
                    unix_error("could not wait");
                }
                int value2 = WEXITSTATUS(status2);
                result = value1 + value2;
            }
        }
    }

    /* Print or return the result */
    if(doPrint == 1){
        printf("%d \n", result);
    } else {
        exit(result);
    }
}


