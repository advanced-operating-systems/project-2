/*
  utcsh - The UTCS Shell

  Tanner Dreiling, trd758
  Xuefei Zhao, xz6784

  9/7/2020 - 9/17/2020
*/

/* Tanner and Sophie both drove the whole file, hard to tell who did what. */

/* Read the additional functions from util.h. They may be beneficial to you
in the future */
#define _GNU_SOURCE
#include "util.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/wait.h>

/* Global variables */

/* The array for holding shell paths. Can be edited by the functions in util.c*/
char shell_paths[MAX_DIRS_IN_PATH][MAX_CHARS_IN_CMDLINE];
static char prompt[] = "utcsh> "; /* Command line prompt */
static char *default_shell_path[2] = {"/bin", NULL};
/* End Global Variables */

/* Convenience struct for describing a command. Modify this struct as you see
 * fit--add extra members to help you write your code. */
struct Command {
  char **args;      /* Argument array for the command */
  char *outputFile; /* Redirect target for file (NULL means no redirect) */
  int numArgs;      /* Length of args */
};

/* Here are the functions we recommend you implement */
char **split_commands(char *userInput, int *numCommands);
char **tokenize_command_line(char *cmdline, int *numTokens);
struct Command parse_command(char **tokens, int *numTokens);
void eval(struct Command *cmd, int *numCommands);
int try_exec_builtin(struct Command *cmd);
void exec_external_cmd(struct Command *cmd);
void exec_external_cmds(struct Command *cmds, int *numCommands, int i);

/* Print error message */
void print_error(){
  char error_message[30] = "An error has occurred\n";
  unsigned int nbytes_written;
  nbytes_written = write(STDERR_FILENO, error_message, strlen(error_message));
  if(nbytes_written != strlen(error_message)){
    exit(2);
  }
}

/* Frees an array of strings when given the length as well. */
void free_array(char ** array, int len){
  int i;
  for(i = 0; i < len; i++){
    free(array[i]);
  }
  free(array);
}

/* Main REPL: read, evaluate, and print. This function should remain relatively
   short: if it grows beyond 60 lines, you're doing too much in main() and
   should try to move some of that work into other functions. */
int main(int argc, char **argv) {

  set_shell_path(default_shell_path);

  /* Declaring variables */
  FILE *stream = NULL;
  char *printedPrompt = NULL;
  char emptyPrompt[] = "";
  int numTokens;
  int numCommands;
  int numValidCommands;

  /* Checking for script reading. */
  if(argc == 1){
    printedPrompt = prompt;
    stream = stdin;
  } else if(argc == 2){
    printedPrompt = emptyPrompt;
    stream = fopen(argv[1], "r");
    if(stream == NULL){
      print_error();
      exit(1);
    }
  } else {
    print_error();
    exit(1);
  }
  
  while (1) {
    /* Initialization */
    printf("%s", printedPrompt);
    numCommands = 0;
    numValidCommands = 0;
    
    /* Read */
    char *userInput = (char *) malloc(MAX_CHARS_IN_CMDLINE * sizeof(char));
    if(userInput == NULL){
      print_error();
      exit(1);
    }
    size_t n = MAX_CHARS_IN_CMDLINE;
    int numChar = getline(&userInput, &n, stream);
    
    if(numChar == -1) {
      /* EOF */
      exit(0);
    } else if(numChar > 1) {
      char **allCommands = split_commands(userInput, &numCommands);
      struct Command *validCommands = NULL;
      validCommands = malloc(numCommands * sizeof(struct Command));
      if(validCommands == NULL){
        print_error();
        exit(1);
      }

      /* Makes tokens from the array of command tokens
         then parses them into command structs */
      int i;
      for(i = 0; i < numCommands; i++){
        numTokens = 0;
        char **tokens = tokenize_command_line(allCommands[i], &numTokens);
        
        if(numTokens > 0) {
          struct Command cmd = parse_command(tokens, &numTokens);
          
          if(cmd.args != NULL){
            /* Add valid commands to the array */
            validCommands[numValidCommands] = cmd;
            numValidCommands++;
          } else {
            /* Invalid commands */
            free_array(tokens, numTokens);
          }

        } else {
          /* No valid tokens */
          free_array(tokens, numTokens);
        }

        free(allCommands[i]);
      }

      free(allCommands);
      
      /* Evaluate */
      if(numValidCommands > 0){
        eval(validCommands, &numValidCommands);
      }
      free(validCommands);
    }
    free(userInput);
  }

  return 0;
}

/** Turn a userinput into command lines with strtok
 * 
 * This function checks for concurrent commands being run, counts the number,
 * and places them into an array of command lines.
 * Returns all the concurrent commands from the userInput.
 */
char ** split_commands(char *userInput, int *numCommands){

  /* Exclude new line character */
  userInput[strlen(userInput) - 1] = '\0';

  /* Count number of commands */
  char *cpy = NULL;
  cpy = malloc((strlen(userInput) + 1) * sizeof(char));
  if(cpy == NULL){
    print_error();
    exit(1);
  }
  strcpy(cpy, userInput);
  char *cmd = NULL;
  for(cmd = strtok(cpy, "&"); cmd != NULL; cmd = strtok(NULL, "&")){
    (*numCommands)++;
  }
  free(cpy);
  cpy = NULL;

  /* Fill in the command lines into an array. */
  char **allCommands = malloc(((*numCommands)) * sizeof(char *));
  if(allCommands == NULL){
    print_error();
    exit(1);
  }
  int i = 0;
  for(cmd = strtok(userInput, "&"); cmd != NULL; cmd = strtok(NULL, "&")){
    allCommands[i] = (char *) malloc((strlen(cmd) + 1) * sizeof(char));
    if(allCommands[i] == NULL){
      print_error();
      exit(1);
    }
    strcpy(allCommands[i], cmd);
    i++;
  }

  return allCommands;
}

/** Turn a command line into tokens with strtok
 *
 * This function turns a command line into an array of arguments, making it
 * much easier to process. First, you should figure out how many arguments you
 * have, then allocate a char** of sufficient size and fill it using strtok()
 */
char **tokenize_command_line(char *cmdline, int *numTokens) {

  /* Count number of arguments */
  char *cpy = malloc(strlen(cmdline) * sizeof(char));
  if(cpy == NULL){
    print_error();
    exit(1);
  }
  cpy = strcpy(cpy, cmdline);
  char *token = NULL;
  for(token = strtok(cpy, " "); token != NULL; token = strtok(NULL, " ")){
    (*numTokens)++;
  }
  free(cpy);
  cpy = NULL;

  /* Fill in the arguments */
  char **tokenArr = malloc(((*numTokens) + 1) * sizeof(char *));
  if(tokenArr == NULL){
    print_error();
    exit(1);
  }
  int i = 0;
  for(token = strtok(cmdline, " "); token != NULL; token = strtok(NULL, " ")){
    tokenArr[i] = (char *) malloc((strlen(token) + 1) * sizeof(char));
    if(tokenArr[i] == NULL){
      print_error();
      exit(1);
    }
    strcpy(tokenArr[i], token);
    i++;
  }
 
  /* Executing an external command requires the last argument to be NULL. */
  char *nullPtr = NULL;
  tokenArr[i] = nullPtr;

  return tokenArr;
}

/** Turn tokens into a command.
 *
 * The `struct Command` represents a command to execute. This is the preferred
 * format for storing information about a command, though you are free to change
 * it. This function takes a sequence of tokens and turns them into a struct
 * Command.
 */
struct Command parse_command(char **tokens, int *numTokens) {

  struct Command cmd;

  /* Locate ">" */
  int i = 0;
  while(i < (*numTokens) && strcmp(tokens[i], ">") != 0){
    i++;
  }

  /* Check for invalid commands */
  /* i != numTokens means it did find a redirect. */
  /* i != (*numTokens) - 2 means there is incorrect number of argument 
     after the redirect symbol. */
  /* i == 0 mean there was no argument BEFORE the redirect symbol. */
  if(i != (*numTokens) && (i != (*numTokens) - 2 || i == 0)){
    print_error();
    cmd.args = NULL;
    cmd.outputFile = NULL;
    cmd.numArgs = 0;
    return cmd;
  }

  /* Check for output file. */
  if(i == (*numTokens)){
    /* No output file */
    cmd.args = tokens;
    cmd.outputFile = NULL;
    cmd.numArgs = (*numTokens);
  } else {
    cmd.outputFile = tokens[i + 1];
    free(tokens[i]);  /* Free ">" */
    tokens[i] = NULL;
    cmd.args = tokens;
    cmd.numArgs = (*numTokens) - 2;
  }

  return cmd;
}

/** Evaluate a single command
 *
 * Both built-ins and external commands can be passed to this function--it
 * should work out what the correct type is and take the appropriate action.
 */
void eval(struct Command *cmd, int *numCommands) {
  int i;

  if(try_exec_builtin(&(cmd[0])) == 0){
    /* External commands */
    exec_external_cmds(cmd, numCommands, 0);
  } else {
    /* Built-in commands */
    for(i = 1; i < (*numCommands); i++) {
      try_exec_builtin(&(cmd[i]));
    }
  }

  /* Free commands */
  for(i = 0; i < (*numCommands); i++) {      
    if(cmd[i].outputFile != NULL){
      free(cmd[i].outputFile);
    }
    free_array(cmd[i].args, cmd[i].numArgs);
  }
}

/** Execute built-in commands
 *
 * If the command is a built-in command, immediately execute it and return 1
 * If the command is not a built-in command, do nothing and return 0
 */
int try_exec_builtin(struct Command *cmd) {

  if(strcmp(cmd->args[0], "exit") == 0){ 
    if(cmd->numArgs != 1){
      print_error();
    } else {
      exit(0);
    }
    return 1;
  } else if(strcmp(cmd->args[0], "cd") == 0){
    if(cmd->numArgs != 2){
      print_error();
    } else {
      if(chdir(cmd->args[1]) == -1){
        print_error();
      }
    }
    return 1;
  } else if(strcmp(cmd->args[0], "path") == 0){    
    if(set_shell_path(&(cmd->args[1])) == 0){
      print_error();
    }
    return 1;
  } else {
    return 0;
  }
}

/* This function allows us to run multiple external commands concurrently.
   It accomplishes this with recursion and forks. cmds is the list of commands,
   numCommands is the number of commands to execute, and
   int i is used to check our placement in the array for the base case.
   This function returns when all the commands have executed.*/
void exec_external_cmds(struct Command *cmds, int *numCommands, int i){

  /* Base case */
  if(i == ((*numCommands) - 1)){
    exec_external_cmd(&(cmds[i]));
  }
  else{
    pid_t fork_value = fork();
    if(fork_value < 0){
      print_error();
    }
    /*if child, execute individual program*/
    else if(fork_value == 0){
      exec_external_cmd(&(cmds[i]));
      exit(0);
    }
    /*if parent, handle the next command in the array with recursion.*/
    else{
      exec_external_cmds(cmds, numCommands, i + 1);
      int status = 0;
      pid_t childPid = wait(&status);
      if(childPid == -1){
        print_error();
      }
    }
  }
}

/** Execute an external command
 *
 * Execute an external command by fork-and-exec. Should also take care of
 * output redirection, if any is requested
 */
void exec_external_cmd(struct Command *cmd) {

  char *exec = NULL;
  
  if(!is_absolute_path(cmd->args[0])){
    int x = 0;
    /* Checks if the requested external program exists in each known path. */
    while(!(exec = exe_exists_in_dir(shell_paths[x], cmd->args[0])) && 
        strcmp(shell_paths[x], "") != 0){
      x++;
    }
    
    if(exec == NULL){
      print_error();
      return;
    }   
  } 
  
  else {
    exec = cmd->args[0];
    int exec_forbidden = access(exec, X_OK);
    if (exec_forbidden) {
      print_error();
      return;
    }
  }
  
  pid_t fork_value = fork();
  if(fork_value < 0) {
    print_error();
  } else if(fork_value == 0){
    /* If there is an output file, it prints the result to it. */
    if(cmd->outputFile != NULL) {
      int fd = open(cmd->outputFile, O_WRONLY | O_CREAT | O_TRUNC, 0666);
      if(fd == -1){
        print_error();
        return;
      }
      if(dup2(fd, 1) == -1){
        print_error();
        return;
      }
      if(dup2(fd, 2) == -1){
        print_error();
        return;
      }
    }
    printf("BEFORE EXEC *%s* *%s*\n", exec, cmd->args[0]);
    execv(exec, cmd->args);
    print_error();
    exit(1);
  } else{
    int status;
    pid_t childPid = wait(&status);
    if(childPid == -1){
      print_error();
    }
  }
  
  return;
}
